# µe

An ANSI tty micro text editor by grunfink - public domain

## Goals

- stripped, size-optimized size of Linux gcc amd64 object < 6K (6144 bytes) [*]
- fixed maximum file size (changeable by a compile-time option)
- undo (fixed levels, changeable by a compile-time option)
- copy/paste
- expects an utf-8 environment (both ANSI terminal and documents), uses iso8859-1 interally
- 1 file
- no syntax highlight

[*] Target was 5k (5120 bytes) but wasn't possible.

## Keys

```
ctrl-a, home    beginning of line
ctrl-b          mark selection
ctrl-c          copy to clipboard
ctrl-d, delete  delete char
ctrl-e, end     end of line
ctrl-h, left    move left
ctrl-j, down    move down
ctrl-k, up      move up
ctrl-l, right   move right
ctrl-n, pgdn    next page
ctrl-p, pgup    previous page
ctrl-q          quit
ctrl-s          save document
ctrl-u          unmark selection
ctrl-v          paste from clipboard
ctrl-x          cut to clipboard
ctrl-y          delete line
ctrl-z          undo
```
